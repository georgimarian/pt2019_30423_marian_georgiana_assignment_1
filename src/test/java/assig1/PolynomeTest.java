package assig1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import logic.Monome;
import logic.Operations;
import logic.Polynome;

public class PolynomeTest {

	@Test
	public void test() {
		Polynome p1 = new Polynome();
		p1.insertTerm(new Monome(2, 3));
		p1.insertTerm(new Monome(1, 6));
		p1.insertTerm(new Monome(0, 4));
		p1.insertTerm(new Monome(5, 5));
		assertEquals("5.0x^5+3.0x^2+6.0x^1+4.0", p1.toString());

		// Derivative test
		Operations op = new Operations();
		Polynome p2 = op.derivate(p1);
		assertEquals("25.0x^4+6.0x^1+6.0", p2.toString());

		// Integration test
		Polynome p3 = op.integrate(p1);
		assertEquals("0.8333333x^6+x^3+3.0x^2+4.0x^1", p3.toString());

		Polynome p4 = op.add(p1, p3);
		assertEquals("0.8333333x^6+5.0x^5+x^3+6.0x^2+10.0x^1+4.0", p4.toString());

		// Inversion test
		Polynome p5 = op.negative(p2);
		assertEquals("-25.0x^4-6.0x^1-6.0", p5.toString());

		// Subtraction test
		Polynome p6 = op.subtract(p1, p2);
		assertEquals("5.0x^5-25.0x^4+3.0x^2-2.0", p6.toString());

		// Addition test
		Polynome p7 = op.add(p1, p2);
		assertEquals("5.0x^5+25.0x^4+3.0x^2+12.0x^1+10.0", p7.toString());

		// Deep copy check
		Polynome p9 = op.add(p1, p3);
		assertEquals(p9.toString(), p4.toString());

		// Multiplication with 0
		Polynome multiplier = new Polynome();
		multiplier.insertTerm(new Monome(0, 0));
		Polynome multiplication = op.multiply(p1, multiplier);
		assertEquals("0", multiplication.toString());
		// Multiplication with x
		Polynome multiplier2 = new Polynome();
		multiplier2.insertTerm(new Monome(1, 1));
		Polynome multiplication2 = op.multiply(p1, multiplier2);
		assertEquals("5.0x^6+3.0x^3+6.0x^2+4.0x^1", multiplication2.toString());

		multiplier2.insertTerm(new Monome(2, 1));
		Polynome multiplication3 = op.multiply(p1, multiplier2);
		assertEquals("5.0x^7+5.0x^6+3.0x^4+9.0x^3+10.0x^2+4.0x^1", multiplication3.toString());

	}

	@Test
	public void divisionTest() {
		Operations op = new Operations();

		Polynome dividend = new Polynome();
		dividend.insertTerm(new Monome(1, 2));
		dividend.insertTerm(new Monome(3, 1));
		dividend.insertTerm(new Monome(0, 4));
		assertEquals("x^3+2.0x^1+4.0", dividend.toString());

		Polynome divisor = new Polynome();
		divisor.insertTerm(new Monome(1, 1));
		divisor.insertTerm(new Monome(0, -3));
		assertEquals("x^1-3.0", divisor.toString());

		Polynome[] result = new Polynome[2];
		result = op.divide(dividend, divisor);
		assertEquals("x^2+3.0x^1+11.0", result[0].toString());
		assertEquals("37.0", result[1].toString());

		Polynome divisor2 = new Polynome();
		divisor2.insertTerm(new Monome(0, 2));
		Polynome[] result2 = new Polynome[2];
		if (divisor2.getHighestDegreeTerm().getCoefficient() == 0) {
			System.out.println("ERROR! Can't divide to 0!");
		} else {
			result2 = op.divide(dividend, divisor2);
			assertEquals("0.5x^3+x^1+2.0", result2[0].toString());
			assertEquals("0", result2[1].toString());
		}

		Monome hiDeg = dividend.getTermOfDegree(3);
		assertEquals("x^3", hiDeg.toString());
	}
}
