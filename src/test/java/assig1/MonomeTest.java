package assig1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import logic.Monome;
import logic.Operations;

public class MonomeTest {

	@Test
	public void test() {
		Monome m1 = new Monome(2, 2);
		Monome m2 = new Monome(2, 3);

		// Addition
		if (m1.getDegree() == m2.getDegree()) {
			Monome m3 = Operations.add(m1, m2);
			assertEquals("5.0x^2", m3.toString());
		}

		// Multiplication
		Monome m4 = Operations.multiply(m1, m2);
		assertEquals("6.0x^4", m4.toString());

		// Derivation
		Monome m5 = Operations.derive(m4);
		assertEquals("24.0x^3", m5.toString());

		// Integration
		Monome m6 = Operations.integrate(m4);
		assertEquals("1.2x^5", m6.toString());

		// Division
		Monome div = new Monome(2, 6);
		Monome m7 = Operations.divide(m5, div);
		assertEquals("4.0x^1", m7.toString());

	}

}
