package view;

import java.awt.event.ActionListener;

import javax.swing.*;

public class View extends JFrame {
	private JTextField firstPolynomial = new JTextField();
	private JTextField secondPolynomial = new JTextField();
	private JTextField resultField = new JTextField(30);
	private JButton addButton;
	private JButton subtractButton;
	private JButton multiplyButton;
	private JButton divideButton;
	private JButton derivateButton;
	private JButton integrateButton;

	public View() {

		setTitle("Polynomial calculator");
		this.setBounds(90, 90, 600, 350);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);

		JLabel firstPolynomialLabel = new JLabel();
		firstPolynomialLabel.setText("Enter first Polynomial");
		firstPolynomialLabel.setBounds(50, 20, 200, 30);
		getContentPane().add(firstPolynomialLabel);

		firstPolynomial.setBounds(50, 50, 500, 30);
		getContentPane().add(firstPolynomial);

		JLabel secondPolynomialLabel = new JLabel();
		secondPolynomialLabel.setText("Enter second Polynomial");
		secondPolynomialLabel.setBounds(50, 80, 200, 30);
		getContentPane().add(secondPolynomialLabel);

		secondPolynomial.setBounds(50, 110, 500, 30);
		getContentPane().add(secondPolynomial);

		addButton = new JButton("Add");
		addButton.setBounds(50, 150, 60, 40);
		getContentPane().add(addButton);

		subtractButton = new JButton("Subtract");
		subtractButton.setBounds(115, 150, 90, 40);
		getContentPane().add(subtractButton);

		multiplyButton = new JButton("Multiply");
		multiplyButton.setBounds(210, 150, 80, 40);
		getContentPane().add(multiplyButton);

		divideButton = new JButton("Divide");
		divideButton.setBounds(295, 150, 70, 40);
		getContentPane().add(divideButton);

		derivateButton = new JButton("Derivate");
		derivateButton.setBounds(370, 150, 90, 40);
		getContentPane().add(derivateButton);

		integrateButton = new JButton("Integrate");
		integrateButton.setBounds(465, 150, 90, 40);
		getContentPane().add(integrateButton);

		JLabel resultLabel = new JLabel("Result");
		resultLabel.setBounds(50, 200, 100, 20);
		getContentPane().add(resultLabel);

		resultField.setBounds(50, 220, 500, 30);
		getContentPane().add(resultField);
	}

	public String getFirstPolynomial() {

		return firstPolynomial.getText();
	}

	public String getSecondPolynomial() {

		return secondPolynomial.getText();
	}

	public void setResult(String result) {
		resultField.setText(result);
	}

	public void addAddButtonActionListener(ActionListener actionListener) {
		addButton.addActionListener(actionListener);
	}

	public void addSubtractButtonActionListener(final ActionListener actionListener) {
		subtractButton.addActionListener(actionListener);
	}

	public void addMultiplyButtonActionListener(final ActionListener actionListener) {
		multiplyButton.addActionListener(actionListener);
	}

	public void addDivideButtonActionListener(final ActionListener actionListener) {
		divideButton.addActionListener(actionListener);
	}

	public void addDerivateButtonActionListener(final ActionListener actionListener) {
		derivateButton.addActionListener(actionListener);
	}

	public void addIntegrateButtonActionListener(final ActionListener actionListener) {
		integrateButton.addActionListener(actionListener);
	}

	public void displayMessage(final String messageText) {
		JOptionPane.showMessageDialog(this, messageText);
	}

}
