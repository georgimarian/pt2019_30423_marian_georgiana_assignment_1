package logic;

public class Monome implements Comparable<Monome> {
	private int degree;
	private float coefficient;

	public Monome(int degree, float coef) {
		this.degree = degree;
		this.coefficient = coef;
	}

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public float getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(float coefficient) {
		this.coefficient = coefficient;
	}

	/*
	 * Multiplies by -1
	 */
	public void invert() {
		this.coefficient = -1 * this.coefficient;
	}

	/*
	 * Prints monomial in readable manner (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		if (this.getDegree() >= 1) {
			if (this.getCoefficient() == 1) {
				stringBuilder.append("x^" + this.getDegree());
			} else {
				stringBuilder.append(this.getCoefficient() + "x^" + this.getDegree());
			}
		} else if (this.getCoefficient() != 0) {
			stringBuilder.append(this.getCoefficient());
		}
		return stringBuilder.toString();
	}

	/**
	 * Compares current Monomial to another one, returning 1 if it is greater, -1 if
	 * smaller, 0 otherwise
	 */
	public int compareTo(Monome m1) {
		if (this.getDegree() > m1.getDegree())
			return 1;
		if (this.getDegree() < m1.getDegree())
			return -1;
		return 0;
	}

}
