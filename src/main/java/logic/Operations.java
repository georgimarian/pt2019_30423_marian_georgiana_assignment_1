package logic;

public class Operations {
	/**
	 * Addition of 2 monomials
	 * 
	 * @param m1
	 * @param m2
	 * @return
	 */
	public static Monome add(Monome m1, Monome m2) {
		return new Monome(m1.getDegree(), m1.getCoefficient() + m2.getCoefficient());
	}

	/**
	 * Subtraction of 2 monomials
	 * 
	 * @param m1
	 * @param m2
	 * @return
	 */
	public static Monome multiply(Monome m1, Monome m2) {
		return new Monome(m1.getDegree() + m2.getDegree(), m1.getCoefficient() * m2.getCoefficient());
	}

	/**
	 * Division of 2 monomials
	 * 
	 * @param m1
	 * @param m2
	 * @return
	 */
	public static Monome divide(Monome m1, Monome m2) {
		return new Monome(m1.getDegree() - m2.getDegree(), m1.getCoefficient() / m2.getCoefficient());
	}

	/**
	 * Derivation of monomial
	 * 
	 * @param m1
	 * @return
	 */
	public static Monome derive(Monome m1) {
		return new Monome(m1.getDegree() - 1, m1.getCoefficient() * m1.getDegree());
	}

	/**
	 * Performs integration of monomial
	 * 
	 * @param m1
	 * @return
	 */
	public static Monome integrate(Monome m1) {
		return new Monome(m1.getDegree() + 1, m1.getCoefficient() / (m1.getDegree() + 1));
	}

	/**
	 * Divides polynomial by constant
	 * 
	 * @param dividend
	 * @param divisor
	 * @return
	 */
	public Polynome divideByConstant(Polynome dividend, Polynome divisor) {
		Polynome result = new Polynome();
		for (Monome m : dividend.getTerms()) {
			result.insertTerm(divide(m, divisor.getHighestDegreeTerm()));
		}
		return result;
	}

	/**
	 * Performs multiplication of a polynomial by a monomial
	 * 
	 * @param p
	 * @param m2
	 * @return
	 */
	public Polynome multiplyByMonome(Polynome p, Monome m2) {
		Polynome result = new Polynome();
		for (Monome m : p.getTerms()) {
			if (m2.getCoefficient() != 0) {
				result.insertTerm(multiply(m, m2));
			}
		}
		return result;
	}

	/**
	 * Performs the addition of two polynomials. A subtraction is also an addition!
	 * 
	 * @param p1
	 * @param p2
	 * @return
	 */
	public Polynome add(Polynome p1, Polynome p2) {
		Polynome result = deepCopy(p1);

		for (Monome m : p2.getTerms()) {
			Monome n = result.getTermOfDegree(m.getDegree());
			if (n != null) {
				Monome sum = add(m, n);
				if (sum.getCoefficient() != 0) {
					result.insertTerm(sum);
				}
				result.getTerms().remove(n);
			} else {
				result.insertTerm(m);
			}
		}

		result.sort();
		return result;

	}

	/**
	 * Subtracts polynomial p2 from p1
	 * 
	 * @param p1
	 * @param p2
	 * @return
	 */
	public Polynome subtract(Polynome p1, Polynome p2) {
		return add(p1, negative(p2));
	}

	/**
	 * Method used for polynomial multiplication. After a term from p1 is multiplied
	 * with the whole p2 it is summed with what we already have in the result.
	 * 
	 * @param p1
	 * @param p2
	 * @return
	 */
	public Polynome multiply(Polynome p1, Polynome p2) {
		Polynome res = new Polynome();
		for (Monome m : p2.getTerms()) {
			Polynome toAdd = new Polynome();
			for (Monome n : p1.getTerms()) {
				if (n.getCoefficient() != 0 && m.getCoefficient() != 0) {
					toAdd.insertTerm(multiply(m, n));
				}
			}
			// Perform addition between result and the result of multiplication with a
			// monomial
			// in order to avoid multiple terms of same degree
			res = add(res, toAdd);
		}
		return res;
	}

	/**
	 * Performs polynomial division Returns an array of 2 Polynomes, the quotient
	 * and the remainder
	 * 
	 * @param dividend
	 * @param divisor
	 * @return
	 */
	public Polynome[] divide(Polynome dividend, Polynome divisor) {
		Polynome[] result = new Polynome[2];
		Polynome quotient = new Polynome();
		// Initialize remainder with the whole dividend
		Polynome remainder = deepCopy(dividend);
		// While the remainder has greater degree than the divisor
		while (remainder.getHighestDegreeTerm().getDegree() >= divisor.getHighestDegreeTerm().getDegree()
				&& divisor.getHighestDegreeTerm().getDegree() != 0) {
			// Divide the highest degree term of remainder to highest degree term of divisor
			Monome m3 = divide(remainder.getHighestDegreeTerm(), divisor.getHighestDegreeTerm());
			// Insert result into quotient
			quotient.insertTerm(m3);
			// Remove from remainder the result of divisor multiplication with the newly
			// added term to quotient
			remainder = add(negative(multiplyByMonome(divisor, m3)), remainder);
		}
		// Treat division with constant separately
		if (divisor.getHighestDegreeTerm().getDegree() == 0) {
			quotient = divideByConstant(dividend, divisor);
			remainder = new Polynome();
		}

		result[0] = quotient;
		result[1] = remainder;
		return result;
	}

	/**
	 * Performs polynomial derivation
	 * 
	 * @param p1
	 * @return
	 */
	public Polynome derivate(Polynome p1) {
		Polynome res = new Polynome();
		for (Monome m : p1.getTerms()) {
			res.insertTerm(derive(m));
		}
		return res;
	}

	/**
	 * Performs polynomial integration
	 * 
	 * @param p1
	 * @return
	 */
	public Polynome integrate(Polynome p1) {
		Polynome res = new Polynome();
		for (Monome m : p1.getTerms()) {
			res.insertTerm(integrate(m));
		}
		return res;
	}

	/**
	 * Multiplies a polynomial by -1
	 * 
	 * @param p
	 * @return
	 */
	public Polynome negative(Polynome p) {
		Polynome result = deepCopy(p);
		for (Monome term : result.getTerms()) {
			term.invert();
		}
		return result;
	}

	/**
	 * Deep-copies a polynomial
	 * 
	 * @param p1
	 * @return
	 */
	private static Polynome deepCopy(Polynome p1) {
		Polynome result = new Polynome();
		for (Monome m : p1.getTerms()) {
			result.insertTerm(new Monome(m.getDegree(), m.getCoefficient()));
		}
		return result;
	}

}
