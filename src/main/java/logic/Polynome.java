package logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Polynome {
	List<Monome> terms;

	public Polynome() {
		terms = new ArrayList<Monome>();
	}

	public List<Monome> getTerms() {
		return terms;
	}

	public void setTerms(List<Monome> terms) {
		this.terms = terms;
	}

	/**
	 * Adds terms to the already existing list of monomials
	 * 
	 * @param m
	 */
	public void insertTerm(Monome m) {
		this.terms.add(m);
	}

	/**
	 * Returns monomial of desired degree, if exists, else returns null
	 * 
	 * @param degree
	 * @return
	 */
	public Monome getTermOfDegree(int degree) {
		for (Monome m : terms) {
			if (m.getDegree() == degree)
				return m;
		}
		return null;
	}

	/**
	 * Returns the term with the greatest degree in polynomial
	 * 
	 * @return
	 */
	public Monome getHighestDegreeTerm() {
		sort();
		return terms.get(0);
	}

	/**
	 * Sorts polynomial terms in reverse order of their degree
	 */
	public void sort() {
		Collections.sort(terms);
		Collections.reverse(terms);
	}

	/*
	 * Prints polynomial in pretty manner (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		sort();
		StringBuilder stringBuilder = new StringBuilder();
		int in = 0;
		for (Monome term : this.getTerms()) {
			if (term.getCoefficient() > 0 && in != 0)
				stringBuilder.append("+");
			stringBuilder.append(term.toString());
			in++;
		}
		if (stringBuilder.length() == 0)
			stringBuilder.append("0");
		return stringBuilder.toString();
	}

}
