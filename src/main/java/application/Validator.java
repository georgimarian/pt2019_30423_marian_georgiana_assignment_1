package application;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import logic.Monome;
import logic.Polynome;

public class Validator {

	private static final String REGEX = "([\\+\\-][\\d]*)([x]*([\\^][\\d]+)*)"; // validates a monomial

	/**
	 * Uses regular expressions to validate a string and divide it into match groups
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public Polynome validateInput(String text) throws Exception {
		// Add a + sign in front in case there is no beginning sign
		String toBeParsed = text.substring(0, 1).equals("-") ? text : ("+" + text);

		// Validate text by enclosing initial regex into a group, thus checking for one
		// or more monomials
		if (!toBeParsed.matches("(" + REGEX + ")*")) {
			throw new Exception("Polynomial not formatted correctly");
		}
		Pattern pattern = Pattern.compile(REGEX, Pattern.MULTILINE);
		Matcher matcher = pattern.matcher(toBeParsed);

		Polynome result = new Polynome();
		// For each found group, add monomial
		while (matcher.find()) {
			// If not empty string matched
			if (!matcher.group(0).isEmpty()) {
				result.insertTerm(stringToMonome(matcher));
			}
		}
		return result;
	}

	/**
	 * Uses Matcher object to divide input into Monomial components Takes cases of
	 * present/absent coefficient/power for both negative and positive values
	 * 
	 * @param m
	 * @return
	 */
	private Monome stringToMonome(Matcher m) {
		int degree;
		float coefficient = 0;

		// If the first group has only + or it is empty, coefficient is 1
		if (m.group(1).equals("+") || m.group(1).isEmpty()) {
			// If the second group is not empty
			if (!m.group(2).equals(""))
				coefficient = 1;
		} else {
			//If the group has only minus
			if (m.group(1).equals("-")) {
				coefficient = -1;
			} else {
				//If the group has a number
				coefficient = Float.parseFloat(m.group(1));
			}
		}
		// If we have no power coefficient
		if (m.group(3) == null) {
			//If second group is x then 
			if (m.group(2).length() == 1) {
				degree = 1;
			} else {
				//If second group empty, degree is 0
				degree = 0;
			}
		} else {
			//We have a degre
			degree = Integer.parseInt(m.group(3).substring(1, 2));
		}

		return new Monome(degree, coefficient);
	}
}
