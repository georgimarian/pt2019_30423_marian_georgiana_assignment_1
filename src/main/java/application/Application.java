package application;

import logic.Operations;
import view.View;

public class Application {
	public static void main(String[] args) {
		View mainView = new View();
		Validator validator = new Validator();
		Operations operations = new Operations();
		Controller mainController = new Controller(mainView, validator, operations);
		mainController.start();
	}
}
