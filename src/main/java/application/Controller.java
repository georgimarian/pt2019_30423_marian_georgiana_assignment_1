package application;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.UIManager;
import logic.Operations;
import logic.Polynome;
import view.View;

public class Controller {
	private View mainView;
	private Validator validator;
	private Operations operations;

	public Controller(View view, Validator validator, Operations operations) {
		this.mainView = view;
		this.validator = validator;
		this.operations = operations;
	}

	public void start() {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		mainView.setLocationRelativeTo(null);
		mainView.setVisible(true);

		initializeButtonListeners();

	}

	private void initializeButtonListeners() {
		final String FIRST_FORMAT_WARNING = "First polynome not formatted correctly";
		final String SECOND_FORMAT_WARNING = "Second polynome not formatted correctly";
		final String FORMAT_WARNING = "Polynomes not formatted correctly";
		final String VALID_WARNING = "You have not entered 2 polynomials!";
		final String PERFORM_WARNING = "Cannot perform operation!";

		mainView.addAddButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final String firstPolynomial = mainView.getFirstPolynomial();
				final String secondPolynomial = mainView.getSecondPolynomial();

				Polynome first = null;
				Polynome second = null;
				try {
					first = validator.validateInput(firstPolynomial);
				} catch (Exception e1) {
					mainView.displayMessage(FIRST_FORMAT_WARNING);
				}
				try {
					second = validator.validateInput(secondPolynomial);
				} catch (Exception e1) {
					mainView.displayMessage(SECOND_FORMAT_WARNING);
				}
				try {
					if (first != null && second != null) {
						Polynome result = operations.add(first, second);
						mainView.setResult(result.toString());
					}
				} catch (Exception e1) {
					mainView.displayMessage(PERFORM_WARNING);
				}

			}
		});

		mainView.addSubtractButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final String firstPolynomial = mainView.getFirstPolynomial();
				final String secondPolynomial = mainView.getSecondPolynomial();

				if (firstPolynomial.length() == 0 || secondPolynomial.length() == 0) {
					mainView.displayMessage(VALID_WARNING);
				}
				Polynome first = null;
				Polynome second = null;
				try {
					first = validator.validateInput(firstPolynomial);
					second = validator.validateInput(secondPolynomial);
					Polynome result = operations.subtract(first, second);
					mainView.setResult(result.toString());
				} catch (Exception e1) {
					mainView.displayMessage(FORMAT_WARNING);
				}

			}
		});

		mainView.addMultiplyButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final String firstPolynomial = mainView.getFirstPolynomial();
				final String secondPolynomial = mainView.getSecondPolynomial();

				if (firstPolynomial.length() == 0 || secondPolynomial.length() == 0) {
					mainView.displayMessage(VALID_WARNING);
				}
				Polynome first = null;
				try {
					first = validator.validateInput(firstPolynomial);
				} catch (Exception e1) {
					mainView.displayMessage(FIRST_FORMAT_WARNING);
				}
				Polynome second = null;
				try {
					second = validator.validateInput(secondPolynomial);
				} catch (Exception e1) {
					mainView.displayMessage(SECOND_FORMAT_WARNING);

				}
				try {
					Polynome result = operations.multiply(first, second);
					mainView.setResult(result.toString());
				} catch (Exception e1) {
					mainView.displayMessage(PERFORM_WARNING);

				}
			}
		});

		mainView.addDivideButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final String firstPolynomial = mainView.getFirstPolynomial();
				final String secondPolynomial = mainView.getSecondPolynomial();

				if (firstPolynomial.length() == 0 || secondPolynomial.length() == 0) {
					mainView.displayMessage(VALID_WARNING);
				}

				Polynome first = null;
				Polynome second = null;

				try {
					first = validator.validateInput(firstPolynomial);
					second = validator.validateInput(secondPolynomial);
					if (first.getHighestDegreeTerm().getDegree() < second.getHighestDegreeTerm().getDegree()) {
						mainView.displayMessage("Degree of 2nd polynomial must be <= than the 1st");
						throw (new Exception());
					}
					if (second.toString().equals("0")) {
						mainView.displayMessage("Can't divide to 0");
						throw (new Exception());
					}
					Polynome[] result = operations.divide(first, second);
					mainView.setResult("Quotient: " + result[0].toString() + " Remainder: " + result[1].toString());
				} catch (Exception e1) {
					mainView.displayMessage(PERFORM_WARNING);
				}

			}
		});

		mainView.addDerivateButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final String firstPolynomial = mainView.getFirstPolynomial();
				final String secondPolynomial = mainView.getSecondPolynomial();

				if (firstPolynomial.length() == 0) {
					mainView.displayMessage("You have not entered the operand polynomials!");
				}
				if (secondPolynomial.length() > 0) {
					mainView.displayMessage("Only the first polynomial will be used as operand!");
				}
				Polynome first = null;
				try {
					first = validator.validateInput(firstPolynomial);
					Polynome result = operations.derivate(first);
					mainView.setResult(result.toString());
				} catch (Exception e1) {
					mainView.displayMessage(FORMAT_WARNING);
				}

			}
		});

		mainView.addIntegrateButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final String firstPolynomial = mainView.getFirstPolynomial();
				final String secondPolynomial = mainView.getSecondPolynomial();

				if (firstPolynomial.length() == 0) {
					mainView.displayMessage("You have not entered the operand polynomials!");
				}
				if (secondPolynomial.length() > 0) {
					mainView.displayMessage("Only the first polynomial will be used as operand!");
				}
				Polynome first = null;
				try {
					first = validator.validateInput(firstPolynomial);
					Polynome result = operations.integrate(first);
					mainView.setResult(result.toString());
				} catch (Exception e1) {
					mainView.displayMessage("Polynomial not formatted correctly");
				}

			}
		});
	}
}
